﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operator_overloading
{
    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        // operator overloading ==
        public static bool operator ==(Point p1, Point p2)
        {
            // p1 == null, p2 == null --> true
            // p1 == null and p2 != null --> false
            // p1 != null and p2 == null --> false
            // ReferenceEquals(p1, null)
            // p1 == null -> will cause infinite loop
            if (p1 is null && p2 is null)
                return true;
            if (p1 is null || p2 is null)
                return false;

            //return p1.X == p2.X && p1.Y == p2.Y; // shorter
            if (p1.X == p2.X && p1.Y == p2.Y)
                return true;
            else
                return false;
        }

        public static Point operator +(Point p1, Point p2)
        {
            Point result = new Point(p1.X + p2.X, p1.Y + p2.Y);
            return result;
        }

        // operator overloading !=
        public static bool operator !=(Point p1, Point p2)
        {
            // bad practice!!
            //if (p1.X == p2.X && p1.Y == p2.Y)
            //    return false;
            //else
            //    return true;

            //return !(p1 == p2); // shorter
            if (p1 == p2)
                return false;
            else
                return true;
        }

        public override bool Equals(object obj)
        {
            // elaboared way:
            //if (obj is null)
            //    return false;

            //if (!(obj is Point))
            //    return false;
            //Point other = (Point)obj;
            //return this == other;

            // short way
            return this == obj as Point;
        }

        public double DistanceFrom00
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.Y, 2) + Math.Pow(this.X, 2));
            }
        }

        // null > null  --> false
        // null == null --> true
        public static bool operator >(Point p1, Point p2)
        {
            if (ReferenceEquals(p1, null) || ReferenceEquals(p2, null))
                return false;

            // sqrt(x^2)
            //return p1.X == p2.X && p1.Y == p2.Y; // shorter
            return p1.DistanceFrom00 > p2.DistanceFrom00;
        }

        public static bool operator <(Point p1, Point p2)
        {
            if (ReferenceEquals(p1, null) || ReferenceEquals(p2, null))
                return false;

            // sqrt(x^2)
            //return p1.X == p2.X && p1.Y == p2.Y; // shorter
            return p1.DistanceFrom00 < p2.DistanceFrom00;
        }

        // home work 
        // implement >= and <=
        public static bool operator <= (Point p1, Point p2)
        {
            if (ReferenceEquals(p1, null) || ReferenceEquals(p2, null))
                return false;
            return p1.DistanceFrom00 <= p2.DistanceFrom00;
        }
        public static bool operator >=(Point p1, Point p2)
        {
            if (ReferenceEquals(p1, null) || ReferenceEquals(p2, null))
                return false;
            return p1.DistanceFrom00 >= p2.DistanceFrom00;
        }
        public override string ToString()
        {
            return $"Point ({X},{Y})";
        }
    }

}

