﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operator_overloading
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine((new Point(1,1)).Equals(null));
            //int x = 1;
            //int y = 1;
            //if (x == y)
            //{
            //    Console.WriteLine("equals");
            //}
            //else
            //{
            //    Console.WriteLine("not equal!");
            //}
            object.Equals(null, null);
            if (ReferenceEquals(null, null))
            {
                Console.WriteLine();
            }
            Point p1 = new Point(1, 4); // 50
            Point p2 = new Point(1, 4); // 80
            // reference euqal
            // operator ==, !=, > , < , >=, <=, !
            // overloading
            // operator overloading
            if (object.ReferenceEquals(p1, p2))
            {
                Console.WriteLine("reference equals");
            }
            else
            {
                Console.WriteLine("reference not equal!");
            }
            if (p1 == p2)
            {
                Console.WriteLine("with operator overloading equals");
            }
            else
            {
                Console.WriteLine("with operator overloading not equal!");
            }
            if (p1.Equals(p2))
            {
                Console.WriteLine("Equals equals");
            }
            else
            {
                Console.WriteLine("Equals not equal!");
            }
            Point p3 = null;
            if (p1 == p3)
            {
                Console.WriteLine("p1 == p3");
            }
            else
            {
                Console.WriteLine("p1 != p3");
            }
            Console.WriteLine(p1);
            Point p4 = p1 + p2;
            Console.WriteLine("====");
            Console.WriteLine($"{p1 + p2} = {p1} + {p2}");
            Console.WriteLine(p4);

            if (p1 > p2)
            {
                Console.WriteLine($"{p1} > {p2}");
            }
            else
            {
                Console.WriteLine($"! {p1} > {p2}");
            }
            if (p1 >= p2)
            {
                Console.WriteLine($"{p1} => {p2}");
            }
            else
            {
                Console.WriteLine($"! {p1} => {p2}");
            }
            if (p1 <= p2)
            {
                Console.WriteLine($"{p1} <= {p2}");
            }
            else
            {
                Console.WriteLine($"! {p1} <= {p2}");
            }
        }

    }
} 

